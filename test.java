package com.max.oa.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Mercedes-X-Phoenix
 * @create 2018年10月19日 20:18
 * @target Working Hard,Marry To Her!
 * @import ★FAMILY★DREAM★
 * 台湾女生冰塵的问题
 **/
public class test {
    public static void main(String[] args) {
		//移除不是java.lang.Number家族的物件
        ArrayList v = new ArrayList();
        v.add(100);
        v.add(3.14);
        v.add(21);
        v.add(100);
        v.add(5.1);
        v.add("Kitty");
        v.add(200);
        v.add(new Object());
        v.add("Snoopy");
        v.add(new BigInteger("1000"));
        System.out.println(v+" ");
        System.out.println();

        Iterator it = v.iterator();
        while (it.hasNext()) {
            Object o = it.next();
            System.out.println(o);
            if (!(o instanceof Number)) {
                it.remove();
            }
        }
        System.out.println(v+" ");
    }
}
